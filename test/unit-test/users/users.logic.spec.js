const sinon = require('sinon');
const expect = require('chai').expect;
const logic = require('./../../../src/users/users.logic');
const UserModel = require('./../../../src/users/users.model');
const mockUser = {
  _id: '5bcbd7fd604020e60bb72df5',
  username: 'juandetoro',
  __v: 0,
  language: 'javascript',
  location: 'madrid',
  updatedAt: '2018-10-23T22:34:07.415Z'
};
const queryParams = {username: 'juan', location: 'madrid', language: 'javacript'};

describe('USER LOGIC', () => {
  describe('GET USERS', () => {
    beforeEach((done) => {
      sinon.stub(UserModel, 'find').resolves([mockUser, mockUser]);
      done();
    });
    afterEach((done) => {
      UserModel.find.restore();
      UserModel.findOneAndUpdate.restore();
      done();
    });
    it('OK', (done) => {
      sinon.stub(UserModel, 'findOneAndUpdate').resolves(mockUser);
      logic.getUsers({...queryParams})
        .then((users) => {
          expect(users).to.be.an('array');
          const user = users[0];
          expect(user).to.have.property('username');
          expect(user).to.have.property('_id');
          done();
        })
        .catch(e=> done(e));
    });
    it('KO', (done) => {
      sinon.stub(UserModel, 'findOneAndUpdate').rejects(new Error('OMG'));
      logic.getUsers({...queryParams})
        .catch((e)=> {
          try {
            expect(e.message).equals('OMG');
            done();
          } catch (e) {
            done(e);
          }

        });
    });
  });
});
