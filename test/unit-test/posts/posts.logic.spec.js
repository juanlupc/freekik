const sinon = require('sinon');
const expect = require('chai').expect;
const logic = require('./../../../src/posts/posts.logic');
const PostModel = require('./../../../src/posts/posts.model');
const newPost = {
  title: 'title',
  body: 'description',
  picture: 'www.web.com',
  taggedUsers: [
    '5bcbd7c2604020e60bb72da4',
    '5bcbd7c2604020e60bb72da4'
  ]
};

const mockPost = {
  deactivated: false,
  taggedUsers: [
    '5bcbd7c2604020e60bb72da4',
    '5bcbd7c2604020e60bb72da4'
  ],
  _id: '5bd0e8e73831a805a277c77e',
  title: 'title',
  body: 'description',
  picture: 'www.web.com',
  creator: '5bcbd7c2604020e60bb72d98',
  createdAt: '2018-10-24T21:49:27.997Z',
  updatedAt: '2018-10-24T21:49:27.997Z'
};


describe('POST LOGIC', () => {
  describe('createPost', () => {
    afterEach((done) => {
      PostModel.create.restore();
      done();
    });
    it('OK', (done) => {
      sinon.stub(PostModel, 'create').resolves(mockPost);
      logic.createPost(newPost)
        .then((post) => {
          expect(post).have.property('_id');
          expect(post).have.property('title');
          expect(post).have.property('body');
          expect(post).have.property('creator');
          expect(post).have.property('picture');
          done();
        })
        .catch((e) => done(e));
    });
    it('KO', (done) => {
      sinon.stub(PostModel, 'create').rejects(new Error());
      logic.createPost(newPost)
        .catch((e) => {
          try {
            expect(e.status).to.equal(409);
            expect(e.message).to.equal('Error saving');
            done();
          } catch(e) {
            done(e);
          }
        });
    });
  });

  describe('getPost', () => {
    afterEach((done) =>{
      PostModel.find.restore();
      done();
    });
    it('OK', (done) => {
      sinon.stub(PostModel, 'find').returns({populate : () => { return Promise.resolve(mockPost);}});
      logic.getPost({_id: mockPost._id})
       .then((post) => {
         expect(post).have.property('_id');
         expect(post).have.property('title');
         expect(post).have.property('body');
         expect(post).have.property('creator');
         expect(post).have.property('picture');
         done();
       })
       .catch((e) => done(e));
    });
    it('KO', (done) => {
      sinon.stub(PostModel, 'find').returns({populate : () => { return Promise.reject(new Error());}});
      logic.getPost({_id: mockPost._id})
       .catch((e) => {
         try {
           expect(e.status).to.equal(500);
           expect(e.message).to.equal('Error DB');
           done();
         }catch(e) {
           done(e);
         }
       });
    });
  });

  describe('updatePost', () => {
    afterEach((done) =>{
      PostModel.update.restore();
      done();
    });
    it('OK', (done) => {
      sinon.stub(PostModel, 'update').resolves({a: 2});
      sinon.stub(PostModel, 'find').returns({populate : () => { return Promise.resolve(mockPost);}});
      const mockUpdatedPost = {...mockPost, title: 'New Title'};
      logic.updatePost({_id: mockPost._id}, {title: mockUpdatedPost.title})
       .then((post) => {
         expect(post).have.property('_id');
         expect(post).have.property('title');
         expect(post).have.property('body');
         expect(post).have.property('creator');
         expect(post).have.property('picture');
         done();
       })
       .catch(e => done(e));
    });
    it('KO', (done) => {
      sinon.stub(PostModel, 'update').rejects(new Error());
      logic.updatePost({_id: mockPost._id})
       .catch((e) => {
         try {
           expect(e.status).to.equal(500);
           expect(e.message).to.equal('Error DB');
           done();
         } catch(e) {
           done(e);
         }
       });
    });
  });
});
