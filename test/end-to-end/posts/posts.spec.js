const chakram = require ('chakram');
const expect = require('chai').expect;
const secret = require('./../../../config').jwt.secret;
const jwt = require('jsonwebtoken');
const uri = 'http://localhost:7000/posts';
const post = {
  title: 'Title',
  body: 'Body Example'
};
describe('POST ENDPOINTS', () => {
  let params, _id, creator;
  before(() => {
    return chakram.get('http://localhost:7000/users?username=tipiwiny')
      .then(() => {
        params = {
          headers: {
            Authorization: `Bearer ${jwt.sign({username: 'tipiwiny'}, secret)}`
          }};
        return params;
      });
  });
  describe(('CREATE POST'), () => {
    it('OK, Status 200', () => {
      return chakram.post(`${uri}`, post, params)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(200);
          expect(reply.body).have.property('_id');
          expect(reply.body).have.property('title');
          expect(reply.body).have.property('body');
          expect(reply.body).have.property('creator');
          _id = reply.body._id;
          creator = reply.body.creator;
        });
    });
    it('KO, Status 400 Bad Request', () => {
      return chakram.post(`${uri}`, {...post, random: 1}, params)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(400);
          expect(reply.body.message).to.equal('Payload ValidationError');
        });
    });
    it('KO, Status 401 Token Not Valid', () => {
      return chakram.post(`${uri}`, {...post})
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(401);
          expect(reply.body.error).to.equal('Token not valid');
        });
    });
  });
  describe(('GET POST'), () => {
    it('OK, Status 200', () => {
      return chakram.get(`${uri}/${_id}`, params)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(200);
          expect(reply.body).have.property('title');
          expect(reply.body).have.property('body');
          expect(reply.body).have.property('creator');
        });
    });
    it('KO, Status 401 Token Not Valid', () => {
      return chakram.get(`${uri}/${_id}`)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(401);
          expect(reply.body.error).to.equal('Token not valid');
        });
    });
  });
  describe(('GET POST OF CREATOR'), () => {
    it('OK, Status 200 by token', () => {
      return chakram.get(`${uri}/creator`, params)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(200);
          const firstPost = reply.body[0];
          expect(firstPost).have.property('_id');
          expect(firstPost).have.property('title');
          expect(firstPost).have.property('body');
          expect(firstPost).have.property('creator');
        });
    });
    it('OK, Status 200 by id creator', () => {
      return chakram.get(`${uri}/creator?id=${creator}`, params)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(200);
          const firstPost = reply.body[0];
          expect(firstPost).have.property('_id');
          expect(firstPost).have.property('title');
          expect(firstPost).have.property('body');
          expect(firstPost).have.property('creator');
        });
    });
    it('KO, Status 401 Token Not Valid', () => {
      return chakram.get(`${uri}/creator`)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(401);
          expect(reply.body.error).to.equal('Token not valid');
        });
    });
  });
  describe(('MODIFY POST'), () => {
    it('OK, Status 200', () => {
      const title = 'New Title';
      return chakram.put(`${uri}/${_id}`, {title}, params)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(200);
          expect(reply.body.title).to.equal(title);
        });
    });
    it('KO, Status 400 Bad Request', () => {
      return chakram.put(`${uri}/${_id}`, {...post, random: 1}, params)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(400);
          expect(reply.body.message).to.equal('Payload ValidationError');
        });
    });
    it('KO, Status 401 Token Not Valid', () => {
      return chakram.put(`${uri}/${_id}`, {...post})
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(401);
          expect(reply.body.error).to.equal('Token not valid');
        });
    });
  });
  describe(('DEACTIVE POST'), () => {
    it('OK, Status 200', () => {
      return chakram.patch(`${uri}/${_id}/deactive`, {}, params)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(200);
          expect(reply.body).to.have.property('message');
          expect(reply.body.message).to.equal('Post has been deactived');
        });
    });
    it('KO, Status 401 Token Not Valid', () => {
      return chakram.patch(`${uri}/${_id}/deactive`)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(401);
          expect(reply.body.error).to.equal('Token not valid');
        });
    });
  });
});
