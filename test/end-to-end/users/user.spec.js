const chakram = require ('chakram');
const expect = require('chai').expect;
const uri = 'http://localhost:7000/users';
describe('USER ENDPOINTS', () => {
  describe(('GET USERS'), () => {
    it('OK, Status 200', () => {
      return chakram.get(`${uri}?username=juan&&location=madrid&language=javascript&limit=5&offset=1`)
        .then((reply) => {
          const { body } = reply;
          expect(body.length).to.equal(5);
          const item = body[0];
          expect(item.language).to.equal('javascript');
          expect(item.location).to.equal('madrid');
          expect(item.username.includes('tip'));
          expect(reply.response.statusCode).to.equal(200);
        });
    });
    it('OK, Status 401', () => {
      return chakram.get(`${uri}?random`)
        .then((reply) => {
          expect(reply.response.statusCode).to.equal(400);
        });
    });
  });
});
