
# FREEKIK TEST

[![JavaScript Style Guide](https://img.shields.io/badge/style-eslint-blue.svg)](https://eslint.org/)
[![Node.js](https://img.shields.io/badge/node-8.12.0-brightgreen.svg)](https://nodejs.org/es/)
[![NPM Version](https://img.shields.io/badge/npm-6.4.1-red.svg)](https://www.npmjs.com/)
[![Docker Version](https://img.shields.io/badge/docker-18.06.1-blue.svg)](https://www.docker.com/)
[![Coverage](https://img.shields.io/badge/coverage-92%25-brightgreen.svg)](https://istanbul.js.org/)
[![Maintainability](https://img.shields.io/badge/maintainability-75%25-brightgreen.svg)](https://github.com/es-analysis/plato)



The technologies used are:
* Node.js
* Express
* Mocha
* Docker
* Mongo

You need to have Docker installed as well.

### Prerequisites
- [Git](https://git-scm.com/)
- [Docker](https://www.docker.com/) Docker & Docker compose
- [Npm](nodejs.org) npm ^6.4.1
In order to interact with the module:
````bash
    $ chmod +x ./do.sh
    $ ./do.sh help        // Show a menu with all the options
    $ ./do.sh build       // npm install & docker build
    $ ./do.sh start       // Run the module
    $ ./do.sh clean       // Stop all containers
    $ ./do.sh test       // Run test
````

### HOW CREATE A JWT
1. Do this request to save one user in DDBB.
```bash
curl -i  -H "Content-Type: application/json" -X GET localhost:7000/users?username=$githubUser
```

2. Visit [https://jwt.io/](https://jwt.io/)  and create a JWT with :
```js

// PAYLOAD
{ username: githubUser }
// SECRET KEY
{ "yourr-256-bit-secret" : freekikCoding }
```
3. Added token in your request:
 ```bash
 #Example
 curl -i  -H "Content-Type: application/json" -H "Authorization: Bearer $token" -X GET localhost:7000/posts/$idPost
```

### HOW RUN TEST

1. Launch
 ```bash
./do.sh test
```

2. When this command is launched , you will can see the result in the path:
 ```bash
./test/report.html
```

Thanks, Juanlu.
