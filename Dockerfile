FROM mhart/alpine-node:8.11.3
# MS Logic
RUN mkdir /app
WORKDIR /app
COPY . /app
EXPOSE 7000
CMD npm start
