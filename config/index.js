module.exports = {
  port: 7000,
  mongo: {
    url: 'localhost',
    database: 'freekik',
    user: 'user',
    password: 'pass',
    port: 27017
  },
  gitHub: {
    url: 'https://api.github.com/search/users'
  },
  jwt: {
    secret: 'freekikCoding'
  }
};
