const UserModel = require('./users.model');
const proxyGitHub = require('./../services/rest.service');

const getUsers = (queryParams, pagination) => {
  return proxyGitHub.get(queryParams)
  .then((items) => {
    return  items.reduce((p, user) => {
      const newUser = { username: user.login};
      if(queryParams.location) {
        newUser.location = queryParams.location;
      }
      if(queryParams.language) {
        newUser.language = queryParams.language;
      }
      return p.then(() => updateUser(newUser));
    },Promise.resolve());
  })
  .then(() => {
    return findUsers(queryParams, pagination);
  })
  .catch((e) => {
    throw e;});
};

const updateUser = (user) => {
  return UserModel.findOneAndUpdate({username: user.username}, { $set: user}, {new: true, upsert: true})
    .then(doc => doc);
};

const findUsers = (query, pagination) => {
  if(query.username) {
    query.username = new RegExp(`^${query.username}`);
  }
  const find = UserModel.find(query);
  if(pagination && pagination.skip && pagination.limit) {
    const skip = pagination.skip * (pagination.limit - 1);
    find.skip(skip).limit(pagination.limit);
  }
  return find
  .then((data) => data);
};

module.exports = {
  getUsers
};
