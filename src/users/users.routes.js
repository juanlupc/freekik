const express = require('express');
const controller = require('./users.controller');
const router = express.Router();
const { Validator } = require('express-json-validator-middleware');
const validator = new Validator({ allErrors: true });
const validate = validator.validate;
const schemas = require('./schemas');

router.get('/', validate({query: schemas.queryParamsUser}),controller.getUsers);

module.exports = router;
