module.exports = {
  type: 'object',
  properties: {
    location: {
      type: 'string',
      minLength: 1
    },
    username: {
      type: 'string',
      minLength: 1
    },
    language:{
      type: 'string',
      minLength: 1
    },
    offset: {
      type: 'string',
      minimum: 1

    },
    limit: {
      type: 'string',
      minimum: 1
    }
  },
  additionalProperties: false
};
