const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    unique: true
  },
  location: String,
  language: String,
},{
  timestamps: true,
  versionKey: false
});

module.exports = mongoose.model('user', UserSchema);
