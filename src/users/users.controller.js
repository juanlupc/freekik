const logic = require('./users.logic');

const getUsers = (req, res) => {
  const pagination = {limit: Number(req.query.limit), skip: Number(req.query.offset)};
  delete req.query.limit;
  delete req.query.offset;
  return logic.getUsers(req.query, pagination)
    .then((data) => {
      return res.status(200).json(data);
    })
    .catch((e) => {
      return res.status(e.status).json({error: e.message});
    });
};

module.exports = {
  getUsers
};
