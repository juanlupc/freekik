const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
const { ValidationError } = require('express-json-validator-middleware');
const config = require('../config');
const users = require('./users');
const posts = require('./posts');
const app = express();
const auth = require('./services/auth.service');
require('./services/mongo.service')();

app.use(cors());
app.use(compression());
app.use(bodyParser.json({limit: '8mb'}));
app.use(bodyParser.urlencoded({limit: '8mb', extended: false}));
// ROUTES
app.use('/users', users);
app.use('/posts', auth.verifyUser, posts);

// Error handler for valication errors
app.use(function (err, req, res, next) {
  if (err instanceof ValidationError) {
    // At this point you can execute your error handling code
    console.error(err.validationErrors);
    res.status(400).json({ message: 'Payload ValidationError'});
    next();
  }
  else next(err); // pass error on if not a validation error
});
app.route('/*').get((req, res) => {
  res.status(404).send({error: 'Path Not Found'});
});

app.listen(config.port, () => {
  console.log(`Freekik App running at ${config.port}`);
});

function callbackExit () {
  console.log('SERVER ERROR', JSON.stringify(arguments));
}

process.on('exit', callbackExit);              // do something when app is closing
process.on('SIGINT', callbackExit) ;            // catches ctrl+c event
process.on('uncaughtException', callbackExit);  // catches uncaught exceptions

// Expose app
module.exports = app;
