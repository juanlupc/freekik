const jwt = require('jsonwebtoken');
const secret = require('./../../config').jwt.secret;
const UserModel = require('./../users/users.model');

const verifyUser = (req, res, next) => {
  const error = {
    status: 401,
    message: 'Token not valid'
  };
  try {
    let token = req.headers.authorization;
    if(!req.headers.authorization) {
      throw error;
    }
    token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, secret);
    return UserModel.findOne({username: decoded.username})
      .then((user) => {
        if(user) {
          req.user = user;
          next();
        } else {
          error.message = 'User not authorized';
          throw error;
        }
      })
      .catch(e => {
        res.status(e.status).json({error: e.message});
      });

  } catch(err) {
    return res.status(error.status).json({error: error.message});
  }
};

module.exports = {
  verifyUser
};
