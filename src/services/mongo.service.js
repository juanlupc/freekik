const mongoConfig = require('./../../config').mongo;
const mongoose = require('mongoose');

module.exports =function(){
  const { database, port, } = mongoConfig;
  mongoose.connect(`mongodb://mongo:${port}/${database}`);

  mongoose.connection.on('connected', function(){
    console.log('Mongoose default connection is open to');
  });

  mongoose.connection.on('error', function(err){
    console.log('Mongoose default connection has occured '+err+' error');
  });

  mongoose.connection.on('disconnected', function(){
    console.log('Mongoose default connection is disconnected');
  });

  process.on('SIGINT', function(){
    mongoose.connection.close(function(){
      console.log('Mongoose default connection is disconnected due to application termination');
      process.exit(0);
    });
  });
};
