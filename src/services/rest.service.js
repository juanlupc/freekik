const createError = require('http-errors');
const client = require('superagent');
const config = require('./../../config');

const emitGetToGitHub = (queryParams) => {
  const completedURL =`${config.gitHub.url}?q=${assemblerQueryParams(queryParams)}`;
  console.log('completedURL', completedURL);
  return new Promise((resolve, reject) => {
    client
      .get(completedURL)
      .set('Accept', 'application/json')
      .end((err, resp) => {
        if (err) {
          reject(createError(500, 'Server Error GitHub'));
        }
        resolve(resp.body.items);
      });
  });
};

const assemblerQueryParams = (queryParams) => {
  let adaptedQueryParams='';
  const { username, location, language } = queryParams;
  let addSymbolAdd = false;
  if(username) {
    addSymbolAdd = true;
    adaptedQueryParams = username.toLowerCase();
  }
  if(location) {
    adaptedQueryParams+=`${addSymbolAdd?'+':''}location%3A${location.toLowerCase()}`;
    addSymbolAdd = true;
  }
  if(language) {
    adaptedQueryParams+=`${addSymbolAdd?'+':''}language%3A${language.toLowerCase()}`;
  }
  return adaptedQueryParams;
};
module.exports = {
  get: emitGetToGitHub
};
