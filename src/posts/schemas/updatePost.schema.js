module.exports = {
  type: 'object',
  properties: {
    title: {
      type: 'string'
    },
    body: {
      type: 'string'
    },
    desactivated:{
      type: 'boolean'
    },
    picture: {
      type: 'string'
    },
    taggedUsers: {
      type: 'array',
      items: {
        type: 'string',
        pattern: '^[0-9a-fA-F]{24}$'
      }
    }
  },
  additionalProperties: false
};
