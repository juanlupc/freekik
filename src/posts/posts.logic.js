const PostModel = require('./posts.model');


const createPost = (post) => {
  return PostModel.create(post)
    .catch(()=> {
      throw { status: 409, message: 'Error saving'};
    });
};

const getPost = (query, pagination) => {
  const find = PostModel.find(query).populate('taggedUsers creator');
  if(pagination && pagination.skip && pagination.limit) {
    const skip = pagination.skip * (pagination.limit - 1);
    find.skip(skip).limit(pagination.limit);
  }
  return find
    .then((data) => data)
    .catch(() => {
      throw { status: 500, message: 'Error DB'};
    });
};

const updatePost = (postID, values) => {
  return PostModel.update({ _id: postID} , values)
  .then(() => getPost({_id: postID}))
  .catch(() => {
    throw { status: 500, message: 'Error DB'};
  });
};


module.exports = {
  createPost,
  getPost,
  updatePost
};
