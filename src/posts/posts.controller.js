const logic = require('./posts.logic');

const createPost = (req, res) => {
  const payload = req.body;
  payload.creator = req.user._id;
  return logic.createPost(payload)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch(e => {
      res.status(e.status).json(e.message);
    });
};

const getPost = (req, res) => {
  const _id = req.params.postID;
  return logic.getPost({_id})
  .then((data) => {
    res.status(200).json(data[0]);
  })
  .catch(e => {
    res.status(e.status).json(e.message);
  });
};

const getPostsByCreator = (req, res) => {
  const creator = req.query.id || req.user._id;
  const pagination = { limit: req.query.limit, offset: req.query.offset};
  return logic.getPost({creator}, pagination)
  .then((data) => {
    res.status(200).json(data);
  })
  .catch(e => {
    res.status(e.status).json(e.message);
  });
};

const updatePost = (req, res) => {
  const { postID } = req.params;
  const newValues = req.body;
  return logic.updatePost(postID, newValues)
  .then((data) => {
    res.status(200).json(data[0]);
  })
  .catch(e => {
    res.status(e.status).json(e.message);
  });
};

const deactivePost = (req, res) => {
  const { postID } = req.params;
  return logic.updatePost(postID, {desactivated: false})
  .then(() => {
    res.status(200).json({message: 'Post has been deactived'});
  })
  .catch(e => {
    res.status(e.status).json(e.message);
  });
};

module.exports = {
  createPost,
  getPost,
  getPostsByCreator,
  updatePost,
  deactivePost
};
