const express = require('express');
const { Validator } = require('express-json-validator-middleware');
const validator = new Validator({ allErrors: true });
const validate = validator.validate;
const schemas = require('./schemas');
const controller = require('./posts.controller');
const router = express.Router();

router.get('/creator', controller.getPostsByCreator);
router.get('/:postID', controller.getPost);
router.post('/', validate({body: schemas.post}),controller.createPost);
router.put('/:postID', validate({body: schemas.updatePost}), controller.updatePost);
router.patch('/:postID/deactive', controller.deactivePost);


module.exports = router;
