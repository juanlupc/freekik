const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
  title: String,
  body: String,
  deactivated: { type: Boolean, default: false},
  picture: String,
  creator:  {type: Schema.Types.ObjectId, ref: 'user', required: true},
  taggedUsers: [{type: Schema.Types.ObjectId, ref: 'user'}],
},{
  timestamps: true,
  versionKey: false
});
PostSchema.methods.create = (post) => {
  const newPost = new this(post);
  return newPost.save();
};
const PostModel = mongoose.model('post', PostSchema);
module.exports = PostModel;
