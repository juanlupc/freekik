#!/bin/sh
# based on: https://ypereirareis.github.io/blog/2015/05/04/docker-with-shell-script-or-makefile/


# Output colors
NORMAL="\\033[0;39m"
RED="\\033[1;31m"
BLUE="\\033[1;34m"

# Names to identify images and containers of this app
IMAGE_NAME='bet-victor'
CONTAINER_NAME="bet-victor"

setup() {
  docker --version >/dev/null 2>&1 || { echo "I require docker but it's not installed.  Aborting." >&2; exit 1; }
  docker-compose --version >/dev/null 2>&1 || { echo "I require docker-compose but it's not installed.  Aborting." >&2; exit 1; }
  git --version >/dev/null 2>&1 || { echo "I require git but it's not installed.  Aborting." >&2; exit 1; }
  node --version >/dev/null 2>&1 || { echo "I require node.js but it's not installed.  Aborting." >&2; exit 1; }
  npm --version >/dev/null 2>&1 || { echo "I require npm but it's not installed.  Aborting." >&2; exit 1; }
  npm i
}

build() {

  npm i
  docker build -t $IMAGE_NAME:latest .

  [ $? != 0 ] && \
    error "Docker image build failed !" && exit 100

  docker rmi $(docker images | grep "^<none>" | awk '{print $3}')
}

run() {
  export TASK=$1
  echo Running in mode: $TASK
  docker-compose up
}

clean() {
  unset TASK
  docker-compose down
}

start() {
  run "start"
}

debug() {
  run "debug"
}

lint() {
  run "lint"
}

test() {
  run "test"
}

cov() {
  run "cov"
}

qa() {
  run "qa"
}

bash() {
  log "BASH"
  docker run -it --rm -v $(pwd):/app $IMAGE_NAME /bin/sh
}

log() {
  echo "$BLUE > $1 $NORMAL"
}

error() {
  echo ""
  echo "$RED >>> ERROR - $1$NORMAL"
}

help() {
  echo "-----------------------------------------------------------------------"
  echo "                      Available commands                              -"
  echo "-----------------------------------------------------------------------"
  echo -e -n "$BLUE"
  echo "   > setup - Install & check dependencies"
  echo "   > build - Build the Docker image in order to use by itself"
  echo "   > start - Run with complementary containers needed to develop"
  echo "   > debug - Run as 'start' but in debug mode with port 5858 open"
  echo "   > test  - Launch tests"
  echo "   > cov   - Launch test and check the covered lines"
  echo "   > qa    - Launch the quality and complexity report"
  echo "   > clean - Stop and removes local containers"
  echo "   > bash -  Log you into main container"
  echo "   > help -  Display this help"
  echo -e -n "$NORMAL"
  echo "-----------------------------------------------------------------------"
}

$*
